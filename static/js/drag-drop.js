/* The dragging code for '.draggable' from the demo above
 * applies to this demo as well so it doesn't have to be repeated. */

// enable draggables to be dropped into this
lines = [];
targets = []
id = 0;

function addItem() {
  console.log("add");
}

function clearLines() {
  console.log("clear");
  removeLines();
  lines=[];
}

function undoLine() {
  lines[lines.length-1].remove();
}

function addItem() {
  var div = document.createElement('div');
  div.id = 'yes-drop-' + id;
  div.className = 'drag-drop';
  div.innerText = '#device-' +id;
  document.getElementById('outer-dropzone').appendChild(div);
  id += 1;
}

interact('.drag-drop')
  .on('tap', function(e){
  console.log(e.currentTarget['id']);
  targets.push(e.currentTarget['id'])
  if (targets.length == 2) {
    console.log("create new line");
    line = new LeaderLine(
      document.getElementById(targets[0]),
      document.getElementById(targets[1])
    );
    lines.push(line);
    targets = [];
  }

})
  .draggable({
    inertia: true,
    restrict: {
      restriction: "parent",
      endOnly: true,
      elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
    },
    autoScroll: true,
    // dragMoveListener from the dragging demo above
    onmove: dragMoveListener,
  });

function dragMoveListener (event) {
    var target = event.target,
        // keep the dragged position in the data-x/data-y attributes
        x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
        y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

    // translate the element
    target.style.webkitTransform =
    target.style.transform =
      'translate(' + x + 'px, ' + y + 'px)';

    // update the posiion attributes
    target.setAttribute('data-x', x);
    target.setAttribute('data-y', y);
    updateLines();
  }

function updateLines(){
  lines.forEach(function(line) {
      line.position()
   });
}

function removeLines() {
  lines.forEach(function(line) {
    line.remove();
  });
}
